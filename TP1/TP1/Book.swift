//
//  Book.swift
//  TP1
//
//  Created by Guillaume Gonzales on 28/10/2020.
//

import Foundation

struct Book {
    var title: String
    var author: String
}
