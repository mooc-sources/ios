//
//  main.swift
//  TP1
//
//  Created by Guillaume Gonzales on 28/10/2020.
//

import Foundation

// Ex. 1
let myInt = 3
let myStr = "This is a string"
print("Integer : \(myInt) - String : \(myStr)")

// Ex. 2
func add(a: Int, b: Int) -> Int {
    return a + b
}
print("3 + 2 = \(add(a: 3, b: 2))")

// Ex. 3
var titles = [String]()
titles.append("Title 1")
titles.append("Title 2")
print(titles)

// Ex. 4
let b1 = Book(title: "Title 1", author: "Author 1")
let b2 = Book(title: "Title 2", author: "Author 2")

var library = [Book]()
library.append(b1)
library.append(b2)

for book in library {
    print(book.title)
}
