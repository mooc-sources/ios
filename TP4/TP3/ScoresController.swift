//
//  ScoresController.swift
//  TP3
//
//  Created by Guillaume Gonzales on 28/10/2020.
//

import UIKit

class ScoresController: UIViewController {
    
    @IBOutlet weak var lb1: UILabel!
    @IBOutlet weak var lb2: UILabel!
    @IBOutlet weak var lb3: UILabel!
    
    var scores: [Int]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let scores = scores else { return }        
        if scores.count > 0 {
            lb1.text = "1 - \(scores[0])"
        }
        if scores.count > 1 {
            lb2.text = "2 - \(scores[1])"
        }
        if scores.count > 2 {
            lb3.text = "3 - \(scores[2])"
        }
    }
    
    @IBAction func okAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
