//
//  ViewController.swift
//  TP3
//
//  Created by Guillaume Gonzales on 28/10/2020.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var numberTF: UITextField!
    @IBOutlet weak var resultLB: UILabel!
    
    var scores = [Int]()
    var congrats: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Game.sharedInstance.reset()
        resultLB.text = "Welcome to the game ! Try to guess the number between 0 & 100"
    }

    @IBAction func playAction(_ sender: Any) {
        Game.sharedInstance.nbRound += 1
        
        guard let value = Int(numberTF.text ?? "") else { return }
        let result: GameStatus? = Game.sharedInstance.checkValue(value: value)
        
        if (result == GameStatus.OVER) {
            resultLB.text = "Secret value is lower"
        } else if (result == GameStatus.UNDER) {
            resultLB.text = "Secret value is higher"
        } else {
            scores.append(Game.sharedInstance.nbRound)
            congrats = "Congratulations !!! You found in \(Game.sharedInstance.nbRound) tries !"
            
            resultLB.text = ""
            
            let alertController = UIAlertController(title: "Congratulations !!!", message: "You found in \(Game.sharedInstance.nbRound) tries !", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (_) in
                self.resetAction(self)
                self.performSegue(withIdentifier: "showEnd", sender: self)
            }
            alertController.addAction(action)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func resetAction(_ sender: Any) {
        Game.sharedInstance.reset()
        
        numberTF.text = ""
        resultLB.text = "Welcome to the game ! Try to guess the number between 0 & 100"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showScores" {
            let dest = segue.destination as! ScoresController
            dest.scores = scores.sorted()
        }
        
        if segue.identifier == "showEnd" {
            let dest = segue.destination as! CongratsController
            dest.congrats = congrats
        }
    }
}

