//
//  CongratsController.swift
//  TP3
//
//  Created by Guillaume Gonzales on 29/10/2020.
//

import UIKit

class CongratsController: UIViewController {
    
    @IBOutlet weak var congratsLB: UILabel!
    
    var congrats: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        congratsLB.text = congrats
    }
    
}
