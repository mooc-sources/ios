# iOS

__TPs__

- TP1 : Simple Swift exercices
- TP2 : Command-line guess number game
- TP3 : Add UI to the game
- TP4 : Add second screen to the game to pass and present scores list
- TP5 : Request _JsonPlaceholder's API_ to fetch and parse __TODO__ Json object



__Project__

* SaleAd : 
  * Simple application providing a UICollectionView to present SaleAds object parsed from the Grails API
  * Contains another screen to create a new Sale advertisment

