//
//  ViewController.swift
//  TP5
//
//  Created by Guillaume Gonzales on 29/10/2020.
//

import UIKit

struct Todo: Decodable {
    let userId: Int
    let id: Int
    let title: String
    let completed: Bool
}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/todos") else { return }
        let task = URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                print("An error occured : ", err)
                return
            }
            
            guard let jsonData = data else { return }
            
            do {
                let todos: [Todo] = try JSONDecoder().decode([Todo].self, from: jsonData)
                print("Success : ", todos.count)
            } catch {
                print("An error occured parsing the Json")
            }
        }
        
        task.resume()
    }
}

