//
//  Game.swift
//  TP2
//
//  Created by Guillaume Gonzales on 28/10/2020.
//

import Foundation

class Game {
    public static var sharedInstance = Game()
    
    var nbRound: Int = 0
    var finalValue: Int = 0
    
    private init() { }

    func reset() {
        nbRound = 0
        finalValue = Int.random(in: 0...100)
    }
    
    func getValue() -> Int {
        let val: String = readLine() ?? ""
        guard let finalVal = Int(val) else { return -1 }
        
        return finalVal
    }

    func checkValue(value: Int) -> GameStatus {
        nbRound += 1
        
        if value < finalValue {
            return GameStatus.UNDER
        } else if value > finalValue {
            return GameStatus.OVER
        } else {
            return GameStatus.EQUAL
        }
    }
    
    func showResult(result: GameStatus) {
        if (result == GameStatus.OVER) {
            print("Secret value is lower")
        } else if (result == GameStatus.UNDER) {
            print("Secret value is higher")
        }
    }
}
