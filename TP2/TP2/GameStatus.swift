//
//  GameStatus.swift
//  TP2
//
//  Created by Guillaume Gonzales on 28/10/2020.
//

import Foundation

enum GameStatus {
    case UNDER
    case EQUAL
    case OVER
}
