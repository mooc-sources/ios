//
//  SaleAdModel.swift
//  SaleAd
//
//  Created by Guillaume Gonzales on 29/10/2020.
//

import Foundation

struct SaleAdModel : Codable {
    let title: String
    let price: String
    let description: String
}
