//
//  SaleAdsCollectionController.swift
//  SaleAd
//
//  Created by Guillaume Gonzales on 29/10/2020.
//

import UIKit

private let reuseIdentifier = "Cell"

class SaleAdsCollectionController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var datasource = [SaleAdModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .lightGray
        navigationItem.title = "Buy & Sale"

        // Register cell classes
        self.collectionView!.register(SaleAdCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
    }
    
    private func fetchData() {
        guard let url = URL(string: "http://192.168.88.227:8095/api/salesad?max=100") else { return }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, resp, err) in
            if let err = err {
                print("Failure : ", err)
                return
            }
            
            guard let jsonData = data else { return }
            do {
                self.datasource = try JSONDecoder().decode([SaleAdModel].self, from: jsonData)
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            } catch {
                print("Error while parsing json")
            }
        }
        
        task.resume()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! SaleAdCell
    
        let sam = datasource[indexPath.row]
        cell.saledAd = sam
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

}
