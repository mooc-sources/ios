//
//  SaleAdCell.swift
//  SaleAd
//
//  Created by Guillaume Gonzales on 29/10/2020.
//

import UIKit

class SaleAdCell: UICollectionViewCell {
    
    var saledAd: SaleAdModel? {
        didSet {
            titleLabel.text = saledAd?.title
            priceLabel.text = "\(saledAd?.price ?? "")€"
            descLabel.text = saledAd?.description
        }
    }
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    
    var priceLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    
    var descLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        let hStackView = UIStackView(arrangedSubviews: [descLabel, priceLabel])
        hStackView.alignment = .center
        hStackView.axis = .horizontal
        hStackView.spacing = 10
        hStackView.distribution = .fillEqually
        
        let stackView = UIStackView(arrangedSubviews: [titleLabel, hStackView])
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.alignment = .fill
        
        addSubview(stackView)
        stackView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
