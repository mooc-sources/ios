//
//  AddSaleAdController.swift
//  SaleAd
//
//  Created by Guillaume Gonzales on 29/10/2020.
//

import UIKit

class AddSaleAdController: UIViewController {
    
    let titleTextField: UITextField = {
        let textField = UITextField()
        textField.font = .systemFont(ofSize: 14)
        textField.placeholder = "Title"
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    let descTextField: UITextField = {
        let textField = UITextField()
        textField.font = .systemFont(ofSize: 14)
        textField.placeholder = "Description"
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    let priceTextField: UITextField = {
        let textField = UITextField()
        textField.font = .systemFont(ofSize: 14)
        textField.placeholder = "Price"
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    let createButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("CREATE", for: .normal)
        button.addTarget(self, action: #selector(handleCreate), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        navigationItem.title = "Create Ad"
        
        setupUI()
    }
    
    private func setupUI() {
        let stackView = UIStackView(arrangedSubviews: [titleTextField, descTextField, priceTextField, createButton])
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        
        view.addSubview(stackView)
        stackView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, width: 0, height: 200)
    }
    
    @objc
    func handleCreate() {
        titleTextField.resignFirstResponder()
        priceTextField.resignFirstResponder()
        descTextField.resignFirstResponder()
        
        guard let title = titleTextField.text else { return }
        guard let price = priceTextField.text else { return }
        guard let desc = descTextField.text else { return }
        
        let newSAM = SaleAdModel(title: title, price: price, description: desc)
        
        do {
            let jsonData = try JSONEncoder().encode(newSAM)
            
            guard let url = URL(string: "http://192.168.88.227:8095/api/salesad") else { return }
            
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = "POST"
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.httpBody = jsonData
            
            URLSession.shared.dataTask(with: urlRequest) { (data, resp, err) in
                if let err = err {
                    print("Failure : ", err)
                    return
                }
                
                DispatchQueue.main.async {
                    self.titleTextField.text = ""
                    self.priceTextField.text = ""
                    self.descTextField.text = ""
                    
                    self.tabBarController?.selectedIndex = 0
                }
            }.resume()
        } catch {
            print("Error while encoding object")
        }
    }
    
    

}
