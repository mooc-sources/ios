//
//  MainTabBarController.swift
//  SaleAd
//
//  Created by Guillaume Gonzales on 29/10/2020.
//

import UIKit

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewControllers()
    }
    
    private func setupViewControllers() {
        
        let collectionLayout = UICollectionViewFlowLayout()
        let navSaleAds = UINavigationController(rootViewController: SaleAdsCollectionController(collectionViewLayout: collectionLayout))
        
        navSaleAds.tabBarItem.image = #imageLiteral(resourceName: "search").withRenderingMode(.alwaysOriginal)
        navSaleAds.tabBarItem.selectedImage = #imageLiteral(resourceName: "search_selected").withRenderingMode(.alwaysOriginal)
        
        let navCreateAd = UINavigationController(rootViewController: AddSaleAdController())
        
        navCreateAd.tabBarItem.image = #imageLiteral(resourceName: "new_offer").withRenderingMode(.alwaysOriginal)
        navCreateAd.tabBarItem.selectedImage = #imageLiteral(resourceName: "new_offer_selected").withRenderingMode(.alwaysOriginal)
        
        viewControllers = [navSaleAds, navCreateAd]
        
    }
}
